#!/bin/bash

export dir="/submarine"
export etc="$dir/etc"
export log="$dir/log"
export run="$dir/run"
export ssl="$dir/ssl"
export www="$dir/www"

sleep $(shuf -i 5-15 -n 1)

# ENVIRONMENT ------------------------------------------------------------------

# WP_DOMAIN
if [[ -z $WP_DOMAIN ]]; then
  echo "ERROR: WP_DOMAIN is not defined."
  exit 1
else
  if [[ $WP_PORT -eq "443" ]];
    then SCHEME=https
    else SCHEME=http
  fi
  export WP_DOMAIN=$(echo $WP_DOMAIN | cut -d, -f1)
  export WP_HOME=${SCHEME}://${WP_DOMAIN}
fi

# DB_HOST
if [[ -z $DB_HOST ]]; then
  if dig mysql > /dev/null;
    then export DB_HOST=mysql
  else echo "ERROR: DB_HOST is not defined." && exit 1
  fi
fi

# DB_NAME
if [[ -z $DB_NAME ]]; then
  if [[ -n $WP_DOMAIN ]];
    then export DB_NAME=`echo ${WP_DOMAIN//./_} | cut -c 1-16`
    else echo "ERROR: DB_NAME is not defined." && exit 1
  fi
fi

# DB_USER
if [[ -z $DB_USER ]]; then
  if env | grep MYSQL_ROOT_PASSWORD > /dev/null;
    then export DB_USER=root
    else echo "ERROR: DB_USER is not defined." && exit 1
  fi
fi

# DB_PASSWORD
if [[ -z $DB_PASSWORD ]]; then
  if env | grep MYSQL_ROOT_PASSWORD > /dev/null;
    then export DB_PASSWORD=`env | grep MYSQL_ROOT_PASSWORD | head -n1 | cut -d= -f2`
    else echo "ERROR: DB_PASSWORD is not defined." && exit 1
  fi
fi

# SETUP ------------------------------------------------------------------------

echo "=> Checking database connection..."
MYSQL="mysql -u$DB_USER -p$DB_PASSWORD -h$DB_HOST"
if $MYSQL -sN -e "SHOW DATABASES" > /dev/null;
  then echo "Connection successfull"
  else echo "ERROR: Database connection failed." && exit 1
fi

if [[ ! -f $dir/.bootstrap ]]; then
  echo "=> Bootstrapping WP cluster"
  source /wps/bin/wp-setup.sh
else
  echo "=> Waiting to join cluster..."
  STATUS="$(cat $dir/.bootstrap)"
  while [[ $STATUS != "done" ]]; do
    sleep 5 && STATUS="$(cat $dir/.bootstrap)"
  done
  echo "Joined $WP_DOMAIN"
fi

# RUN --------------------------------------------------------------------------

chown -R submarine:nginx $dir
exec su -l submarine -c "s6-svscan $run"
